(function ($, Drupal) {

    'use strict';

    /**
     * Call picturefill so newly added responsive images are processed.
     */
    Drupal.behaviors.responsiveBackgroundFieldFormatter = {
        attach: function () {

            var setBackgrounds = function () {
                $('[data-responsive-background-image]').each(function (delta, element) {
                    var responsiveImagesMapping = JSON.parse($(this).attr('data-responsive-background-image'));
                    var newImageUrl = '';

                    $.each(responsiveImagesMapping, function (mediaQuery, imageUrl) {
                        if (matchMedia(mediaQuery).matches) {
                            newImageUrl = imageUrl;
                        }
                    });

                    $(this).css('background-image', 'url(' + newImageUrl + ')');
                });
            };

            var debounce = function(func, wait) {
                var timeout, timestamp;
                var later = function() {
                    var last = (new Date()) - timestamp;

                    if (last < wait) {
                        timeout = setTimeout(later, wait - last);
                    } else {
                        timeout = null;
                        func();
                    }
                };

                return function() {
                    timestamp = new Date();

                    if (!timeout) {
                        timeout = setTimeout(later, wait);
                    }
                };
            };

            $(window).on('resize', debounce(function () {
                setBackgrounds();
            }, 300));

            setBackgrounds();

        }
    };

})(jQuery, Drupal);


