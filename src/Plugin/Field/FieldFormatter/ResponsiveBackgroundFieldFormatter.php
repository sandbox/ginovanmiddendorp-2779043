<?php

namespace Drupal\responsive_background_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;
use Drupal\Core\Cache\Cache;

/**
 * Plugin for responsive background field formatter.
 *
 * @FieldFormatter(
 *   id = "responsive_background",
 *   label = @Translation("Responsive background"),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class ResponsiveBackgroundFieldFormatter extends ResponsiveImageFormatter {

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $elements = [];
        $files = $this->getEntitiesToView($items, $langcode);

        // Early opt-out if the field is empty.
        if (empty($files)) {
            return $elements;
        }

        // Create the markup for each file.
        foreach ($files as $delta => $file) {
            $responsive_image_style = $this->responsiveImageStyleStorage->load($this->getSetting('responsive_image_style'));
            $image_styles_to_load = [];
            $cache_tags = [];

            $breakpoint_mapping = [];

            // Get image styles to load.
            if ($responsive_image_style) {
                $cache_tags = Cache::mergeTags($cache_tags, $responsive_image_style->getCacheTags());
                $image_styles_to_load = $responsive_image_style->getImageStyleIds();

                $breakpoint_manager = \Drupal::service('breakpoint.manager');

                $all_breakpoints = $breakpoint_manager->getDefinitions();

                foreach ($responsive_image_style->getImageStyleMappings() as $mapping) {
                    $breakpoint_mapping[$mapping['image_mapping']] = $all_breakpoints[$mapping['breakpoint_id']];
                }
            }

            // Get the cache tags.
            $image_styles = $this->imageStyleStorage->loadMultiple($image_styles_to_load);
            foreach ($image_styles as $image_style) {
                $cache_tags = Cache::mergeTags($cache_tags, $image_style->getCacheTags());
            }

            // Build the responsive background attribute and convert it to json.
            $responsive_background_attribute = [];
            foreach ($image_styles as $image_style_name => $image_style) {
                $url = entity_load('image_style', $image_style_name)->buildUrl($file->getFileUri());
                $responsive_background_attribute[$breakpoint_mapping[$image_style_name]['mediaQuery']] = $url;
            }

            $responsive_background_json = json_encode($responsive_background_attribute, JSON_UNESCAPED_UNICODE);

            // Construct the render array.
            $render = [
                '#type' => 'html_tag',
                '#tag' => 'div',
                '#attributes' => [
                    'data-responsive-background-image' => $responsive_background_json,
                ],
                '#attached' => [
                    'library' => [
                        'responsive_background_field_formatter/responsive_formatter'
                    ],
                ],
                '#cache' => [
                    'tags' => $cache_tags,
                ]
            ];

            $elements[] = $render;
        }

        return $elements;
    }
}
